package mdtext

import (
	"io"
	"log"
	"strings"

	"github.com/bbrks/wrap/v2"
	bf "github.com/russross/blackfriday/v2"
)

// The textWriter can indent and wrap individual "blocks" of text,
// accumulated with writeString().
type textWriter struct {
	prefixes      []string
	firstPrefixes []string
	curBlock      string
	wrapSize      int
}

func (tw *textWriter) pushPrefix(firstPfx, pfx string) {
	// Make the old rightmost first entry same as the non-first
	// one. This is a special case that pretty much only applies
	// to list items, where we desire only the single, rightmost
	// bullet point to be visible.
	if len(tw.firstPrefixes) > 0 {
		tw.firstPrefixes[len(tw.firstPrefixes)-1] = tw.prefixes[len(tw.prefixes)-1]
	}
	tw.firstPrefixes = append(tw.firstPrefixes, firstPfx)
	tw.prefixes = append(tw.prefixes, pfx)
}

func (tw *textWriter) popPrefix() {
	tw.firstPrefixes = tw.firstPrefixes[:len(tw.firstPrefixes)-1]
	tw.prefixes = tw.prefixes[:len(tw.prefixes)-1]
}

func (tw *textWriter) prefixLen() int {
	var l int
	for _, p := range tw.prefixes {
		l += len(p)
	}
	return l
}

func (tw *textWriter) writeString(_ io.Writer, s string) {
	tw.curBlock += s
}

func (tw *textWriter) emitBlock(w io.Writer, doWrap bool) {
	s := tw.curBlock

	if doWrap {
		n := tw.wrapSize - tw.prefixLen()
		if n < 10 {
			n = 10
		}
		// Remove newlines eventually embedded within the
		// text, effectively ignoring breaks in the paragraph.
		s = strings.Replace(s, "\n", " ", -1)
		s = wrap.Wrap(s, n)
	} else {
		s = strings.TrimSpace(s)
	}
	empty := true
	for idx, line := range strings.Split(s, "\n") {
		if line == "" {
			if !doWrap {
				io.WriteString(w, "\n") // nolint
			}
			continue
		}
		prefixes := tw.firstPrefixes
		if idx > 0 {
			prefixes = tw.prefixes
		}
		for _, p := range prefixes {
			io.WriteString(w, p) // nolint
		}
		io.WriteString(w, line) // nolint
		io.WriteString(w, "\n") // nolint
		empty = false
	}
	if !empty {
		io.WriteString(w, "\n") // nolint
	}

	tw.curBlock = ""
}

// Text renderer for Markdown.
type textRenderer struct {
	*textWriter
}

// NewTextRenderer creates a new blackfriday.Renderer that renders
// Markdown to well-formatted plain text (with line length wrapped at
// wrapSize).
func NewTextRenderer(wrapSize int) bf.Renderer {
	if wrapSize < 1 {
		wrapSize = 75
	}
	return &textRenderer{
		textWriter: &textWriter{
			wrapSize: wrapSize,
		},
	}
}

func (r *textRenderer) RenderNode(w io.Writer, node *bf.Node, entering bool) bf.WalkStatus {
	switch node.Type {

	case bf.BlockQuote:
		if entering {
			r.pushPrefix("> ", "> ")
		} else {
			r.emitBlock(w, true)
			r.popPrefix()
		}

	case bf.CodeBlock:
		r.pushPrefix("    ", "    ")
		r.writeString(w, string(node.Literal))
		r.emitBlock(w, false)
		r.popPrefix()

	case bf.Del:
		break

	case bf.Document:
		break

	case bf.Emph:
		r.writeString(w, "*")

	case bf.Hardbreak:
		r.writeString(w, "\n")
		r.emitBlock(w, false)

	case bf.Heading:
		if entering {
			switch node.Level {
			case 1:
				r.writeString(w, "# ")
			case 2:
				r.writeString(w, "## ")
			case 3:
				r.writeString(w, "### ")
			case 4:
				r.writeString(w, "#### ")
			case 5:
				r.writeString(w, "##### ")
			case 6:
				r.writeString(w, "###### ")
			}
		} else {
			r.emitBlock(w, true)
		}

	case bf.HTMLBlock, bf.HTMLSpan:
		break

	case bf.HorizontalRule:
		r.writeString(w, "-------------------------------------------------------------")
		r.emitBlock(w, false)

	case bf.Image:
		break

	case bf.Item:
		if entering {
			r.pushPrefix("* ", "  ")
		} else {
			r.emitBlock(w, true)
			r.popPrefix()
		}

	case bf.Link:
		r.writeString(w, string(node.LinkData.Destination))
		return bf.SkipChildren

	case bf.List:
		if node.IsFootnotesList {
			return bf.SkipChildren
		}

	case bf.Paragraph:
		if !entering {
			r.emitBlock(w, true)
		}

	case bf.Softbreak:
		break

	case bf.Strong:
		r.writeString(w, "**")

	case bf.Table, bf.TableBody, bf.TableCell, bf.TableRow:
		break

	case bf.Code, bf.Text:
		r.writeString(w, string(node.Literal))

	default:
		log.Printf("unknown node type %v", node.Type)
	}

	return bf.GoToNext
}

func (r *textRenderer) RenderHeader(w io.Writer, ast *bf.Node) {
}

func (r *textRenderer) RenderFooter(w io.Writer, ast *bf.Node) {
}
