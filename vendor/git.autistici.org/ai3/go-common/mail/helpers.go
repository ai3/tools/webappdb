package mail

import (
	"git.autistici.org/ai3/go-common/mail/message"
	"git.autistici.org/ai3/go-common/mail/template"
)

// SendPlainTextMessage sends a simple plaintext message to the
// specified recipient.
func (m *Mailer) SendPlainTextMessage(templateName, lang, subject, rcpt string, values map[string]interface{}) error {
	tpl, err := template.New(templateName, lang, values)
	if err != nil {
		return err
	}
	msg := message.NewText("text/plain", tpl.Text())
	return m.Send(msg, rcpt, subject)
}

// SendTextAndHTMLMessage builds a multipart/alternative message with
// both a plaintext and a HTML part, and sends it to the recipient.
func (m *Mailer) SendTextAndHTMLMessage(templateName, lang, subject, rcpt string, values map[string]interface{}) error {
	tpl, err := template.New(templateName, lang, values)
	if err != nil {
		return err
	}
	msg := message.NewMultiPart(
		"multipart/alternative",
		message.NewText("text/plain", tpl.Text()),
		message.NewText("text/html", tpl.HTML()),
	)
	return m.Send(msg, rcpt, subject)
}
