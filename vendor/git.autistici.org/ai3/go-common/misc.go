package common

import (
	"crypto/x509"
	"fmt"
	"io/ioutil"
)

// LoadCA loads a file containing CA certificates into a x509.CertPool.
func LoadCA(path string) (*x509.CertPool, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cas := x509.NewCertPool()
	if !cas.AppendCertsFromPEM(data) {
		return nil, fmt.Errorf("no certificates could be parsed in %s", path)
	}
	return cas, nil
}
