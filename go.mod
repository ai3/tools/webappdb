module git.autistici.org/ai3/tools/webappdb

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210110180225-a05c683cfe23
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/go-cmp v0.5.4
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/matttproud/golang_protobuf_extensions v1.0.2-0.20181231171920-c182affec369 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
