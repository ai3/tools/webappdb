package webappdb

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"

	"git.autistici.org/ai3/go-common/serverutil"
	proto "git.autistici.org/ai3/tools/webappdb/proto"
)

var sqlStatements = map[string]string{

	"wipe_shard": `
DELETE FROM apps WHERE shard = ?
`,
	"insert_app": `
INSERT INTO apps (site, shard, path, name, version, safe_version, state, vulninfo, timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
`,
	"find_apps_by_site": `
SELECT
  id, site, shard, path, name, version, safe_version, state, vulninfo, timestamp
FROM
  apps
WHERE
  site = ?
`,
	"find_apps_by_name": `
SELECT
  id, site, shard, path, name, version, safe_version, state, vulninfo, timestamp
FROM
  apps
WHERE
  name = ?
`,
	"find_apps_by_version": `
SELECT
  id, site, shard, path, name, version, safe_version, state, vulninfo, timestamp
FROM
  apps
WHERE
  name = ? AND version = ?
`,
}

// Server for the web-applications-db HTTP API.
type Server struct {
	homedirs homedirMap

	db    *sql.DB
	stmts StatementMap
}

// New returns a new Server.
func New(database *sql.DB, homedirMapPath string) (*Server, error) {
	var homedirs homedirMap
	if homedirMapPath != "" {
		var err error
		if homedirs, err = loadHomedirMap(homedirMapPath); err != nil {
			return nil, err
		}
	}

	stmts, err := NewStatementMap(database, sqlStatements)
	if err != nil {
		return nil, err
	}

	return &Server{
		homedirs: homedirs,
		db:       database,
		stmts:    stmts,
	}, nil
}

// Close the Server and all associated resources.
func (s *Server) Close() {
	s.stmts.Close()
}

// Post-process the incoming App objects before they are saved
// to the database. Use this function to add whatever metadata
// augmentation logic we desire.
func (s *Server) processApp(app *proto.App) {
	if site, ok := s.homedirs.Find(app.Path); ok {
		app.Site = site
	}
}

func (s *Server) insertApps(ctx context.Context, shard string, batch []*proto.App) error {
	return retryBusy(ctx, func() error {
		return withTX(ctx, s.db, func(tx *sql.Tx) error {
			if _, err := s.stmts.Get(tx, "wipe_shard").Exec(shard); err != nil {
				return err
			}

			for _, app := range batch {
				s.processApp(app)
				_, err := s.stmts.Get(tx, "insert_app").Exec(
					app.Site,
					shard, // or app.Shard, should be the same.
					app.Path,
					app.Name,
					app.Version,
					app.SafeVersion,
					app.State,
					app.VulnInfo,
					app.Timestamp,
				)
				if err != nil {
					return err
				}
			}
			return nil
		})
	})
}

func (s *Server) findApps(ctx context.Context, queryName string, args ...interface{}) []*proto.App {
	var out []*proto.App
	err := withReadonlyTX(ctx, s.db, func(tx *sql.Tx) error {
		rows, err := s.stmts.Get(tx, queryName).Query(args...)
		if err != nil {
			return err
		}
		defer rows.Close()

		for rows.Next() {
			var app proto.App
			var rowID int64
			if err := rows.Scan(&rowID, &app.Site, &app.Shard, &app.Path, &app.Name, &app.Version, &app.SafeVersion, &app.State, &app.VulnInfo, &app.Timestamp); err != nil {
				log.Printf("Scan error: %v", err)
				continue
			}
			out = append(out, &app)
		}

		return rows.Err()
	})
	if err != nil {
		return nil
	}
	return out
}

func (s *Server) findAppsBySite(ctx context.Context, site string) []*proto.App {
	return s.findApps(ctx, "find_apps_by_site", site)
}

func (s *Server) findAppsByVersion(ctx context.Context, name, version string) []*proto.App {
	if version == "" {
		return s.findApps(ctx, "find_apps_by_name", name)
	}
	return s.findApps(ctx, "find_apps_by_version", name, version)
}

func (s *Server) countApps(ctx context.Context, req *proto.CountAppsRequest) (*proto.CountAppsResponse, error) {
	if req.Limit == 0 {
		req.Limit = 20
	}

	// This is just a query builder.
	groupFields := []string{"state"}
	if req.GroupByName {
		groupFields = append(groupFields, "name")
	}
	if req.GroupByVersion {
		groupFields = append(groupFields, "version")
	}
	if req.GroupBySite {
		groupFields = append(groupFields, "site")
	}
	if len(groupFields) == 0 {
		return nil, errors.New("no group_by criteria provided")
	}

	var whereStr string
	if req.VulnerableOnly {
		whereStr = "WHERE state = 'vulnerable'"
	}

	query := fmt.Sprintf(
		"SELECT COUNT(*) AS c, %s FROM apps %s GROUP BY %s ORDER BY c DESC LIMIT %d",
		strings.Join(groupFields, ", "),
		whereStr,
		strings.Join(groupFields, ", "),
		req.Limit,
	)

	// Now execute the query and fetch the results into a CountAppsResponse.
	var results []proto.CountResult
	err := withReadonlyTX(ctx, s.db, func(tx *sql.Tx) error {
		rows, err := tx.Query(query)
		if err != nil {
			return err
		}
		for rows.Next() {
			var cr proto.CountResult
			cr.Keys = make([]string, len(groupFields))
			// Assemble an array of pointers to the elements of cr.Keys.
			scanArgs := []interface{}{
				&cr.Count,
			}
			for i := 0; i < len(cr.Keys); i++ {
				scanArgs = append(scanArgs, &cr.Keys[i])
			}
			if err := rows.Scan(scanArgs...); err != nil {
				return err
			}
			results = append(results, cr)
		}
		if err := rows.Close(); err != nil {
			return err
		}
		return rows.Err()
	})
	if err != nil {
		return nil, err
	}
	return &proto.CountAppsResponse{
		Fields:  groupFields,
		Results: results,
	}, nil
}

// HTTP handlers

func (s *Server) handleSubmission(w http.ResponseWriter, r *http.Request) {
	var req proto.SubmissionRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	if err := s.insertApps(r.Context(), req.Shard, req.Entries); err != nil {
		log.Printf("submission error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("received %d entries from shard %s", len(req.Entries), req.Shard)

	serverutil.EncodeJSONResponse(w, nil)
}

func (s *Server) handleFindAppsBySite(w http.ResponseWriter, r *http.Request) {
	var req proto.FindAppsBySiteRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	results := make(map[string][]*proto.App)
	for _, site := range req.Sites {
		results[site] = s.findAppsBySite(r.Context(), site)
	}

	serverutil.EncodeJSONResponse(w, &proto.FindAppsBySiteResponse{
		Apps: results,
	})
}

func (s *Server) handleFindAppsByVersion(w http.ResponseWriter, r *http.Request) {
	var req proto.FindAppsByVersionRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	results := s.findAppsByVersion(r.Context(), req.Name, req.Version)
	serverutil.EncodeJSONResponse(w, &proto.FindAppsByVersionResponse{
		Apps: results,
	})
}

func (s *Server) handleCountApps(w http.ResponseWriter, r *http.Request) {
	var req proto.CountAppsRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	resp, err := s.countApps(r.Context(), &req)
	if err != nil {
		log.Printf("error: %+v: %v", req, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	serverutil.EncodeJSONResponse(w, resp)
}

// Handler returns a http.Handler for the server API. All URL paths
// start with /api/.
func (s *Server) Handler() http.Handler {
	h := http.NewServeMux()
	h.HandleFunc("/api/search/by_site", s.handleFindAppsBySite)
	h.HandleFunc("/api/search/by_version", s.handleFindAppsByVersion)
	h.HandleFunc("/api/count", s.handleCountApps)
	h.HandleFunc("/api/submission", s.handleSubmission)
	return h
}
