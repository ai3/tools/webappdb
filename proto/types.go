package webappdb

import "time"

// App stores information about an application instance. The JSON
// fields should match the XML fields as used by 'freewvs --xml'.
type App struct {
	Shard       string    `json:"shard"`
	Path        string    `json:"directory"`
	Site        string    `json:"site"`
	Name        string    `json:"appname"`
	Version     string    `json:"version"`
	SafeVersion string    `json:"safeversion"`
	State       string    `json:"state"`
	VulnInfo    string    `json:"vulninfo"`
	Timestamp   time.Time `json:"timestamp"`
}

// SubmissionRequest RPC.
type SubmissionRequest struct {
	Shard   string `json:"shard"`
	Entries []*App `json:"entries"`
}

// FindAppsBySiteRequest RPC, handles multiple sites in a single
// request to minimize RPC latency.
type FindAppsBySiteRequest struct {
	Sites []string `json:"sites"`
}

// FindAppsByVersionRequest RPC, handles multiple sites in a single
// request to minimize RPC latency.
type FindAppsByVersionRequest struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

// FindAppsBySiteResponse is the response type for FindAppsBySiteRequest.
type FindAppsBySiteResponse struct {
	Apps map[string][]*App `json:"apps"`
}

// FindAppsByVersionResponse is the response type for FindAppsByVersionRequest.
type FindAppsByVersionResponse struct {
	Apps []*App `json:"apps"`
}

// CountAppsRequest requests various types of aggregates.
type CountAppsRequest struct {
	VulnerableOnly bool `json:"vulnerable_only"`
	GroupByName    bool `json:"group_by_name"`
	GroupByVersion bool `json:"group_by_version"`
	GroupBySite    bool `json:"group_by_site"`
	Limit          int  `json:"limit"`
}

type CountResult struct {
	Keys  []string `json:"keys"`
	Count int      `json:"count"`
}

type CountAppsResponse struct {
	Fields  []string      `json:"fields"`
	Results []CountResult `json:"results"`
}
