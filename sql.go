package webappdb

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strings"
	"time"

	migrate "github.com/golang-migrate/migrate/v4"
	msqlite3 "github.com/golang-migrate/migrate/v4/database/sqlite3"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
	"github.com/mattn/go-sqlite3"

	"git.autistici.org/ai3/tools/webappdb/migrations"
)

const dbDriver = "sqlite3"

// OpenDB opens a SQLite database and runs the database migrations.
func OpenDB(dburi string) (*sql.DB, error) {
	if !strings.Contains(dburi, "?") {
		dburi += "?cache=shared&_busy_timeout=10000&_journal=WAL&_mutex=full"
	}

	db, err := sql.Open(dbDriver, dburi)
	if err != nil {
		return nil, err
	}

	if err = runDatabaseMigrations(db); err != nil {
		db.Close() // nolint
		return nil, err
	}

	return db, nil
}

type migrateLogger struct{}

func (l migrateLogger) Printf(format string, v ...interface{}) {
	log.Printf("db: "+format, v...)
}

func (l migrateLogger) Verbose() bool { return true }

func runDatabaseMigrations(db *sql.DB) error {
	si, err := bindata.WithInstance(bindata.Resource(
		migrations.AssetNames(),
		func(name string) ([]byte, error) {
			return migrations.Asset(name)
		}))
	if err != nil {
		return err
	}

	di, err := msqlite3.WithInstance(db, &msqlite3.Config{
		MigrationsTable: msqlite3.DefaultMigrationsTable,
		DatabaseName:    "usermetadb",
	})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("bindata", si, dbDriver, di)
	if err != nil {
		return err
	}
	m.Log = &migrateLogger{}

	log.Printf("running database migrations")
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	return nil
}

// A StatementMap holds named compiled statements.
type StatementMap map[string]*sql.Stmt

// NewStatementMap compiles the given named statements and returns a
// new StatementMap.
func NewStatementMap(db *sql.DB, statements map[string]string) (StatementMap, error) {
	stmts := make(map[string]*sql.Stmt)
	for name, qstr := range statements {
		stmt, err := db.Prepare(qstr)
		if err != nil {
			return nil, fmt.Errorf("error compiling statement '%s': %v", name, err)
		}
		stmts[name] = stmt
	}
	return StatementMap(stmts), nil
}

// Close all resources associated with the StatementMap.
func (m StatementMap) Close() {
	for _, s := range m {
		s.Close() // nolint
	}
}

// Get a named compiled statement (or nil if not found), and associate
// it with the given transaction.
func (m StatementMap) Get(tx *sql.Tx, name string) *sql.Stmt {
	return tx.Stmt(m[name])
}

func withTX(ctx context.Context, db *sql.DB, f func(*sql.Tx) error) error {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	if err := f(tx); err != nil {
		tx.Rollback() // nolint
		return err
	}
	return tx.Commit()
}

func withReadonlyTX(ctx context.Context, db *sql.DB, f func(*sql.Tx) error) error {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback() // nolint
	return f(tx)
}

func isBusy(err error) bool {
	switch e := err.(type) {
	case sqlite3.Error:
		return e.Code == 5
	default:
		return false
	}
}

var defaultQueryTimeout = 3 * time.Second

func retryBusy(ctx context.Context, f func() error) error {
	deadline, ok := ctx.Deadline()
	if !ok {
		deadline = time.Now().Add(defaultQueryTimeout)
	}

	for time.Now().Before(deadline) {
		if err := f(); !isBusy(err) {
			return err
		}

		// Random sleep, max 1ms.
		time.Sleep(time.Duration(rand.Float64()) * time.Millisecond)
	}
	return errors.New("query timed out waiting to lock the database")
}
