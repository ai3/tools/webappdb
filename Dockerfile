FROM golang:latest AS build

ADD . /src
WORKDIR /src
RUN go test -v ./... && \
    go build -o /webappdb-server ./cmd/webappdb && \
    strip /webappdb-server

# We need to use Debian as the base because we need to dlopen libsqlite.
FROM debian:buster
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends libsqlite3-0
COPY --from=build /webappdb-server /usr/bin/webappdb-server

ENTRYPOINT ["/usr/bin/webappdb-server"]
