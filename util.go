package webappdb

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strings"
)

// The homedirMap maps home directories to their associated web
// sites. Site identifiers are opaque strings and can be anything.
// Note that a site can't have the root (/) as a home directory.
type homedirMap map[string]string

func loadHomedirMap(path string) (homedirMap, error) {
	return loadMap(path)
}

// Find the site whose home directory contains path.
func (m homedirMap) Find(path string) (site string, ok bool) {
	for path != "" {
		site, ok = m[path]
		if ok {
			break
		}

		// We use filepath.Split instead of filepath.Dir
		// because it has simpler semantics on the returned
		// dir value and it's easier to check when we're done.
		path, _ = filepath.Split(path)
		path = strings.TrimRight(path, "/")
	}
	return
}

// The site owners map maps site names to owner email addresses.
type siteOwnerMap map[string]string

func loadSiteOwnerMap(path string) (siteOwnerMap, error) {
	return loadMap(path)
}

func loadMap(path string) (map[string]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var m map[string]string
	if err := json.NewDecoder(f).Decode(&m); err != nil {
		return nil, err
	}
	return m, nil
}
