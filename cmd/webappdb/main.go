package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"git.autistici.org/ai3/go-common/mail"
	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/tools/webappdb"
	"gopkg.in/yaml.v2"
)

// Type that wraps together all the server configuration pieces that
// aren't expressed by command-line flags, so that it can be read from
// a YAML file.
type serverConfig struct {
	HomedirMap   string       `yaml:"homedir_map_file"`
	SiteOwnerMap string       `yaml:"site_owner_map_file"`
	NotifyAddr   string       `yaml:"notify_addr"`
	Mail         *mail.Config `yaml:"mail"`

	HTTPConfig *serverutil.ServerConfig `yaml:"http_server"`
}

var (
	addr       = flag.String("addr", ":3420", "tcp `address` to listen to")
	configPath = flag.String("config", "/etc/webappdb/server.yml", "configuration `file`")
	dbPath     = flag.String("db", "/var/lib/webappdb/apps.db?_journal=WAL", "database `path`")
)

// Build the server.Config object out of command-line flags.
func loadConfig() (*serverConfig, error) {
	data, err := ioutil.ReadFile(*configPath)
	if err != nil {
		return nil, err
	}
	var config serverConfig
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func run() error {
	database, err := webappdb.OpenDB(*dbPath)
	if err != nil {
		return fmt.Errorf("database initialization failed: %v", err)
	}
	defer database.Close()

	config, err := loadConfig()
	if err != nil {
		return fmt.Errorf("could not read server configuration: %v", err)
	}

	srv, err := webappdb.New(database, config.HomedirMap)
	if err != nil {
		return err
	}
	defer srv.Close()

	return serverutil.Serve(srv.Handler(), config.HTTPConfig, *addr)
}

func main() {
	log.SetFlags(0)
	flag.Parse()
	if flag.NArg() != 0 {
		log.Printf("wrong number of arguments")
		os.Exit(2)
	}

	if err := run(); err != nil {
		log.Printf("error: %v", err)
		os.Exit(1)
	}
}
