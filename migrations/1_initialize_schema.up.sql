
CREATE TABLE apps (
       id INTEGER PRIMARY KEY,
       site TEXT,
       shard TEXT,
       path TEXT,
       name TEXT,
       version TEXT,
       safe_version TEXT,
       state TEXT,
       vulninfo TEXT,
       timestamp DATETIME,
       notified BOOL NOT NULL DEFAULT 0,
       resolved BOOL NOT NULL DEFAULT 0
);

CREATE INDEX idx_apps_site ON apps (site);
CREATE INDEX idx_apps_state ON apps (state);
CREATE INDEX idx_apps_shard ON apps (shard);
CREATE INDEX idx_apps_shard_path ON apps (shard, path);
